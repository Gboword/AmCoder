# 内容简介

> **主要分享一些在工作或者学习中所遇到的一些问题以及记录一些学习到的东西等！**

# 记录周期

> **不定期记录，持续性更新**

# 访客记录

<a href="https://info.flagcounter.com/McnV"><img src="https://s01.flagcounter.com/count2/McnV/bg_FFFFFF/txt_000000/border_CCCCCC/columns_3/maxflags_15/viewers_3/labels_1/pageviews_1/flags_0/percent_0/" alt="Flag Counter" border="0"></a>

# 作者信息

> :man: Amcoder(:email: ranzechen@dingtalk.com)

> :man: KinseyGe(:email: 692295951@qq.com)

> :man: SunKindoms(:email: 1028633173@qq.com)

> :man: hua_wang(:email: 2812591669@qq.com)

> :man: 阿柳(:email: 35031550@qq.com)

> :man: Gbo(:email: gbo0328@163.com)

> :man: jzh(:email: jzh0302l@163.com)

> :thumbsup: ***一个人的力量毕竟有限，同时也欢迎志同道合的朋友联系我加入进来，一起学习共同进步，通过记录笔记来记录自己的成长过程。这是一个漫长和需要日积月累沉淀知识的过程***

# 转载分享

!> **转载请遵循以下声明，谢谢！** </br> 作者：I'm a Coder. </br>链接：www.amcoder.cn

# 参与贡献

?> 1. 如果您对本项目有任何建议或发现文中内容有误的，欢迎提交 issues 进行指正。 </br> 2. 对于文中我没有涉及到知识点，欢迎[![Fork me on Gitee](https://gitee.com/AmCoder/AmCoder/widgets/widget_4.svg)](https://gitee.com/AmCoder/AmCoder)并提交PR。


## 必备知识

1. 需要Git协同开发的一些常用命令
2. 需要了解常见的MarkDown语法(当然用现成的工具也可以)

## 编写规范

!> **重要的事情说三遍：*请严格遵守markdown语法！请严格遵守markdown语法！请严格遵守markdown语法！***</br></br>***初次提交？***</br> 1.&nbsp;&nbsp;将远程仓库中的项目 **fork** 并 **clone** 到本地，按照markdown的语法风格编写</br>2.&nbsp;&nbsp;将编写好的文档放在**待归档目录**下的**待处理文档**中，进行提交 **PR**，待审核后可归档到**展示目录列表中**和**待归档目录**下的**已处理文档**中</br></br> ***需要修改原来提交过的文档？***</br>1.&nbsp;&nbsp;找到**待归档目录**下的**已处理文档**中的文档进行编辑和修改，修改完毕后需要再次手动移动到**待归档目录**下的**待处理文档**中进行提交即可</br></br>***需要系列记录？***</br>1.&nbsp;&nbsp;如果当前系列记录中没有你想要的目录名称可以联系管理员新增，如果已经存在你需要的系列目录名称，请按照正常上面流程进行提交和修改。**并在提交信息中注明需要归档的系列目录路径**</br></br>***好麻烦？以上操作后的结果？***</br>1.&nbsp;&nbsp;由于本站非博客管理性质类的系统，所以提交的文档都需要进行审核</br>2.&nbsp;&nbsp;针对审核后的文档归档后可能会和原已存在的文档进行合并，此目的是站在知识的角度我们要做到**取其精华**，同时合并后的文档顶部也会注明合作作者</br></br>***关于文档？***</br>1.&nbsp;&nbsp;建议篇幅不要过长，简短意赅</br>2.&nbsp;&nbsp;建议排版清晰，编写思路清晰而非只是方便一时之快</br>3.&nbsp;&nbsp;针对学习系列记录，建议模块化管理，方便查找</br>4.&nbsp;&nbsp;针对晦涩难懂的地方建议衡量好中文释义，用最通俗易懂的说法来注释</br></br>***图片存储在哪里？***</br>1.&nbsp;&nbsp;这里采用的是[图床](https://imgchr.com/)作为远程链接图片的存储

## 本地调试

1. 安装npm命令，npm其实是Node.js的包管理工具，说白了就是安装node.js[点我](https://nodejs.org/dist/v12.18.3/node-v12.18.3-x64.msi) :point_left:
2. 安装的时候即可勾选配置系统环境变量path，不然还得自己手动配置，安装完成后可在cmd中执行命令`npm -v`，显示版本即为安装成功
3. 安装docsify，在cmd中执行`npm i docsify-cli -g`即可
4. 进入到 **Amcoder项目目录中** 在cmd中执行命令(serve后面有个点)`docsify serve .`，会在本地启动一个3000的端口(*http://localhost:3000*)，即实现了本地运行，接下来你就可以一边编写md一边看效果了(实时生效)
