**Session和Cookie**

- Session
> session是一种记录用户状态的机制，不同的是cookie保存在客户端，而session保存在服务端。客户端浏览器发起请求后，服务端会把客户端的信息以某种形式进行记录在服务器上。这就是session。session通过sessionID来识别用户的唯一性，通过cookie将sessionID带到服务器上进行校验。

  - Session创建过程
  >当程序需要为某个客户端的请求创建sessionID时，服务器首先检查这个客户端的请求中是否含有sessionID，如果已经包含则说明此客户端之前创建过session，服务器就按照sessionID把这个session检索出来使用(检索不到会重新创建),如果客户单请求不包含sessionID，则服务器就会为客户端重新创建一个与此次session(会话)相关联的sessionID，然后在本次响应中返回给客户端保存。(sessionID的值是一个既不会重复，又不容易找到规律模仿仿造的字符串。)

- Cookie
> cookie由于http是一种无状态协议，服务器从网络连接上无法知道用户身份，当在页面进行一系列的操作后，服务器无法判断是用户A还是用户B的操作，所以这时就引入了cookie作为类似一个“通行证”的东西，来识别是用户A的操作还是用户B的操作，这就是cookie的工作原理。

  - Cookie创建过程
  > 当客户端请求服务器，如果服务器需要记录该用户的状态，就使用response向客户端浏览器颁发一个cookie。客户端会将cookie保存起来，当浏览器再次请求该网站时，浏览器把请求的网址以及cookie一起交给服务器，服务器核查cookie，以此来辨别用户的状态。服务器还可以根据需求对cookie进行修改。cookie实际上是一小段文本信息，主要内容包括名字，值，过期时间，路径，域。

- 个人总结
  > 我个人认为，可以说cookie是客户端用户的身份验证的身份证，而sessionID则是标识用户的唯一性的身份证号，而session则是服务端用来验证用户sessionID(身份证号码)的验证器。

  - 区别
    - cookie的数据保存在客户端的浏览器上
    - session的数据保存在服务器上
    - cookie不是很安全，其他人可进行分析仿造本地的cookie进行cookie欺骗
    - session会在一定时间内保存在服务器上，当访问增多，会增加服务器的压力。
    - 单个cookie中保存的数据不能超过4k，很多浏览器都限制一个站点最多保留20个cookie
    - 出于安全考虑，可以将登陆信息等重要信息保存在session中，其他信息需要保留可以存在到cookie中。
