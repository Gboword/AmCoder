# 入门练习

1. 引入vue.js库，最好放在head标签中，避免页面出现抖屏的情况
2. 练习
  ```HTML
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="UTF-8">
  		<title>Vue练习</title>
  		<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js" type="text/javascript" charset="utf-8"></script>
  	</head>

  	<body>
  		<!--入门练习-->
  		<div id="test">
  			<p>hello {{content}}</p>
  			<p>
  				<span>hello</span>
  				<span v-text="content2"><span>
  			</p>
  			<div @click="clickTest">
  				{{clickContent}}
  			</div>

  			<input id="username_id" :placeholder="placeholder_content"/>
  		</div>
  	</body>
  	<script type="text/javascript">
  		new Vue({
  			el: '#test',
  			//template: '<h1>我是实例里面的模板</h1>',
  			data: {
  				content:"一般写法",
  				content2: "vue 模板指令写法",
  				clickContent: "点击事件绑定练习---点我改变",
  				placeholder_content: "请您输入用户名"
  			},
  			methods:{
  				clickTest: function(){
  					this.clickContent = "点击事件绑定练习---改变后的结果"
  				}
  			},
  		})
  	</script>
  </html>

    <!--
      new Vue：创建一个实例
      挂载点：el所对应的dom节点
      data：vue实例中的数据，数据的key可以自定义，但是需要对应插值表达式里面的值
      {{}}：获取值，采用的是插值表达式
      模板：挂载点内部的内容称为模板
        模板不仅可以卸载dom元素中还可以写在实例中，template: '<h1>我是实例里面的模板</h1>'

      除了差值表达式以外的模板指令写法：
        v-text：对应v-html将标签转义，显示纯文本
        v-html：不对dom进行转义
        v-on:事件函数="clickTest"：绑定事件(v-on: 可以简写为@)
          实现方式：
            在实例中新增methods方法，并实现绑定的函数clickTest

      使用vue指令的好处是多数情况下是面向数据编程，而非是面向dom编程
    -->
  ```

# 属性绑定(v-bind)和双向数据绑定(v-model)

1. 属性绑定 **:属性**
  > v-bind:属性="变量名称"，其中v-bind:可以缩写为冒号
  >> 实例中需要在data中指定变量对应的值

```HTML
  <!-- 通过实例来绑定input标签的placeholder属性 -->
  <input id="username_id" :placeholder="placeholder_content"/>
  <script type="text/javascript">
    new Vue({
  			el: '#username_id',
  			data: {
  				placeholder_content: "请您输入用户名"
  			}
  	})
  </script>
```

2. 双向绑定 **v-model**
  > 它负责监听用户的输入事件，从而更新数据，并对一些极端场景进行一些特殊处理。同时关联多个dom
  >> v-model="变量名称"

```HTML
  <!-- 在input标签中输入内容的同时回显到p标签中 -->
	<div id="test">
		<p><input v-model="inputContent"/></p>
		<p>我是上面input框中的内容,它变我也变，双向绑定:<span style="color: red;">{{inputContent}}</span></p>
	</div>
  <script type="text/javascript">
    new Vue({
      el: "#test",
      data: {
        inputContent: "",
      }
    })
  </script>
  ```

# 计算属性(computed)和侦听器(watch)

1. 计算属性 **computed**
> 通过某两个属性来计算出另一个属性，而且性能较高，当a,b不修改时，结果c使用的是上一次结果的缓存值。只有依赖的属性发生了变化才会重新计算结果。**这里的属性指的是实例中数据项的属性而非dom标签的属性，不要混淆**

```HTML
  <!-- 计算a加b的结果并赋值给c -->
  <div id="test">
    <p><input v-model="a"/></p>
    <p><input v-model="b"/></p>
    <p>我是a和b相加的结果{{c}}</p>
  </div>
  <script type="text/javascript">
    new Vue({
      el: "#test",
      data:{
        a: "",
        b: ""
      }
      computed:{
        c:function(){
          return this.a + this.b
        }
      }
    })
  </script>
```

2. 侦听器 **watch**
> 监听某个属性发生的变化

```HTML
  <!-- 监听a或者b中任意改变的次数 -->
  <div id="test">
    <p><input v-model="a"/></p>
    <p><input v-model="b"/></p>
    <p>我是a和b相加的结果{{c}}</p>
    <p>我被改变过{{count}}次</p>
  </div>
  <script type="text/javascript">
    new Vue({
      el: "#test"
      data:{
        a: "",
				b: "",
				count: 0
      }
      watch: {
				c: function(){
					this.count++
				}
			}
    })
  </script>
```

# v-if,v-show,v-for

1. v-if v-show
> v-if 和 v-show 区别
>> 使用v-if，如果为false，则是直接移除dom元素来体现</br>而使用v-show则是不会被删除，会添加display:none样式

```HTML
  <div id="test">
    <p v-if="showOrHide">点击下面的按钮来控制我显示或者是隐藏</p>
    <button @click="showOrHideFun">点我试试</button>
  </div>
  <script type="text/javascript">
    new Vue({
      el: '#test',
      data: {
        showOrHide: true
      },
      methods: {
        showOrHideFun: function(){
          return this.showOrHide = !this.showOrHide;
        }
      }
    })
  </script>
```

2. v-for
> 用于数据列表做循环展示

```HTML
  <div id="test">
    <ul>
      <!-- item为dataList中的元素，dataList为变量数组 -->
    	<li v-for="item of dataList">{{item}}</li>
    </ul>
  </div>
  <script type="text/javascript">
    new Vue({
      el: "#test",
      dataList: [1,2,3,4,5]
    })
  </script>
```

# 定义全局组件和局部组件 component

> **注意：** 在vue中每一个组件都是一个实例</br>定义组件，组件名称必须不含大写字母</br>***父组件向子组件交互通过属性的方式，子组件使用props来接收 props:['属性名称']<br/>子组件向父组件交互通过发布订阅模式，如$emit 子组件发布一个事件，然后父组件订阅事件(也就是绑定这个事件，通过实现方法来实现)***

```HTML
<!-- 定义全局组件 -->
<todo-item></todo-item>
<script type="text/javascript">
  Vue.component("todo-item", {
      template: "<ul>hehe</ul>"
  })
</script>
```

```HTML
<!-- 定义局部组件 -->
<part-item></part-item>
<script type="text/javascript">
  var partItem = {
    template: "<ul>我是一个局部组件</ul>"
  }
  new Vue({
    el: '#test',
    components: {
      'part-item': partItem
    }
  })
</script>
```

# vue-cli

1. 需要按照npm和node命令
2. 在vue-cli中，data的值不再是对象而是一个函数
```HTML
<script type="text/javascript">
  export default{
    data (){
      return {
        // 数据
      }
    }
  }
</script>
```
