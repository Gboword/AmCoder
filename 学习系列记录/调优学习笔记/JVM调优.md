!> commit by **SunKindoms**

# JDK体系
![6.png](https://s1.ax1x.com/2020/07/28/aE0v2n.png)   

# JVM整体架构
- Java虚拟者阵营: SUN,HostSpot VM, Apache Harmony,google.....  

## 类执行流程

![7.png](https://s1.ax1x.com/2020/07/28/aEBV2R.png)  

## JVM架构图

![8.png](https://s1.ax1x.com/2020/07/28/aEBmKx.png)  

**完整的java虚拟机是由三本分组成:**  
- 类装载子系统
- 运行时数据区(内存模型)
- 执行引擎  

**示例:**  

- main方法虚拟机执行流程:

![9.png](https://s1.ax1x.com/2020/07/28/aEBGRA.png)

## 虚拟机

**一个方法对应一块栈帧内存区域**

![10.png](https://s1.ax1x.com/2020/07/28/aEB0Ig.png)  

- **java虚拟机会给每个线程分配独立的虚拟机栈,这个栈空间是线程独享**

![11.png](https://s1.ax1x.com/2020/07/28/aEDz7T.png)  

- **字节码文件**  

![12.png](https://s1.ax1x.com/2020/07/28/aEDTAS.png)  

- **运行时数据区-程序计数器和方法区讲解**  
![13.png](https://s1.ax1x.com/2020/07/28/aEDW6I.png)  

- **GC ROOT根节点:** 类加载器,Thread,虚拟机栈的本地变量表,static成员,常量引用,本地方法栈的变量等  
![14.png](https://s1.ax1x.com/2020/07/28/aEDcfH.png)  

- **堆内存**  
![15.png](https://s1.ax1x.com/2020/07/28/aEDalR.png)  

- **垃圾回收机制**  
![16.png](https://s1.ax1x.com/2020/07/28/aED1mV.png)  

- **JMM 内存模型**
![17.png](https://s1.ax1x.com/2020/07/28/aEDmWj.png)


- **JMM内存模型详解**  
  - JMM数据原子操作
    - read(读取) 从主内存读取数据
    - load(载入) 将主内存读取到的数据写入工作内存
    - use(使用) 从工作内存读取数据来计算
    - assign(赋值) 将计算好的值从新赋值到工作内存中
    - store(存储) 将工作内存数据写入主内存
    - write(写入) 将store过去的变量值赋值给内存中的变量
    - lock(锁定) 将主内存变量加锁,标识为线程独占状态
    - unlock(解锁) 将主内存变量解锁,解锁后其他线程可以锁定该变量  

- **JMM 程序执行内存模型示例**
![18.png](https://s1.ax1x.com/2020/07/28/aEBXdO.png)

- **JMM 缓存不一致问题**    
![19.png](https://s1.ax1x.com/2020/07/28/aEB2LV.png)  
