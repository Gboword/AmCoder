!> commit by **SunKindoms**

> 索引对查询速度的影响
>> mysql中提高性能的最有效的方式就是对数据表设计合理的索引。索引提供了高效访问数据的方法，并且加快了查询的速度。因此，索引对查询的速度有着至关重要的影响。使用所以可以快速的定位到表中的某条记录，从而提高数据库查询的速度，提高数据库的性能。如果查询的时候没有使用索引，查询语句将扫描表中的所有记录。在数据量大的情况下，这样查询的速度会很慢。如果使用索引进行查询，查询语句可以根据索引快速定位到待查询的记录，从而减少查询的记录数，达到提高查询速度的目的。

- 举例说明：使用索引和不使用索引的区别

![索引字段](https://s1.ax1x.com/2020/07/30/auawDI.png)

- 数据库总条数为 ***17933*** 条

```SQL
--不使用索引查询
EXPLAIN SELECT * FROM sjzx_catalog_manage where SCM_CATALOG_NAME ='企业目录测试';
-- 分析：不使用索引查询rows查询到17933条，没有命中索引字段
```
![结果图](https://s1.ax1x.com/2020/07/30/auNDsI.png)  

```SQL
-- 使用索引查询
EXPLAIN SELECT * FROM sjzx_catalog_manage where SCM_CATALOG_NAME ='企业目录测试';
-- 分析:使用索引字段,命中精确查询到rows为113条
```
![结果](https://s1.ax1x.com/2020/07/30/auUTcd.png)

- 索引使用的几种特殊情况

  > 索引可以提高查询的速度，但并不是使用带有索引的字段查询时，索引都会起到作用。下面是几种比较特殊的情况。在这些情况下，有可能使用带有索引的字段查询时，索引并没有起到作用

  - 使用like关键字的查询语句
  > 在使用like关键字进行查询的查询语句中，如果匹配字符串的第一个字符为"%"，索引不会起作用。只有"%"不在第一个位置，索引才会起到作用

    - 使用索引字段like查询,字段 **前** 用% ;用like时左边必须是固定值，通配符只能出现在右边

      ```SQL
      EXPLAIN SELECT * FROM log_message where LM_DO_METHOD  like '%showLogMessage'
      ```
      ![结果查询](https://s1.ax1x.com/2020/07/30/auwnmR.png)

      > 分析：使用索引字段like查询,索引没生效,查询到的rows为17933条,数据库总条数为17933条

    ---

    - 使用索引字段like查询,字段 **后** %;用like时左边必须是固定值，通配符只能出现在右边
      ```SQL
      EXPLAIN SELECT * FROM log_message where LM_DO_METHOD  like 'showLogMessage%'
      ```
      ![结果查询](https://s1.ax1x.com/2020/07/30/au0Iat.png)

      > 分析：命中索引,%在后like查询,精确查询到rows为113条,按照上面的sql执行计划参数对应说明

- mysql有多列索引时查询
  - 使用多列索引的查询语句。mysql可以为多个字段创建索引。一个索引可以包括16个字段。对于多列索引，只有查询条件中使用了这些字段中第1个字段的时候，索引才会被使用

- 使用or关键字的查询语句
  - 使用or关键字的查询语句。使用语句的查询条件中只有or关键字，且or前后的两个条件中的列都有索引时，查询中才使用索引。否则，查询将不适用索引

## MYSQL索引最左前缀的解释

 - 在创建一个n列的索引时，遵循“最左前缀”原则

 > 假设有如下表： create table TEST (a varchar2(32) ,b varchar2(32),c date);
 >> 在a和c列上建普通索引： create index IN_TEST on TEST (a, c);
      - 索引左前缀性的第一层意思：必须用到索引的第一个字段。`select * from TEST where b=:xxx and c=sysdate;`则不会用到索引，因为必须有a出现在where 语句中才会使用到该索引。
      - 索引前缀性的第二层意思：对于索引的第一个字段，用like时左边必须是固定值，通配符只能出现在右边。`select * from TEST where a like '1%';`会用到索引;而`select * from TEST where a like '%1';`不会用到索引。
      - 索引前缀性的第三层意思：如果在字段前加了函数，则索引会被抑制，例如：select * from TEST where trim(a)=1,则不会用到索引。
      在字段前嵌入了表达式，索引也将被抑制。假设a是date格式的，那么where a+7<sysdate将不会用到索引，而where a<sysdate-7会用到索引。

      - 还有两个特殊声明：
        - `select * from TEST where a=:xxx and c=sysdate`与`select * from TEST where c=sysdate and a=:xxx;`都会用到索引，即与where语句中字段出现的顺序无关；
        - `select * from TEST where a=:xxx and b=1;`会使用索引，此时A出现，即使其他字段不是索引字段也会使用到索引。
