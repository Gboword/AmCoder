> Elasticsearch的访问风格遵循Restful Api，所以其接口方法决定使用方法，PUT 插入数据，POST 带参请求数据，GET 无参请求数据，Delete 删除数据

- 为某个索引的某个字段,设置ik_max_word分词
  - 请求方式 `PUT`
  ```
  {
      "settings": {
          "index.analysis.analyzer.default.type": "ik_max_word"
      },
      "mappings": {
          "doc": {
              "properties": {
                  "file_content": {
                      "type": "text",
                      "analyzer": "ik_max_word",
                      "search_analyzer": "ik_max_word"
                  }
              }
          }
      }
  }'
  ```
