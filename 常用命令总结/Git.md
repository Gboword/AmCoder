!> commit by **AmCoder** and **KinseyGe**

- Git的提交模式

>本地代码-->待提交列表-->本地仓库-->远程仓库

- Git的三种状态

![image](https://s1.ax1x.com/2020/05/28/tZZGtg.png)

  ```
  modified --> staged --> committed
  ```

- 创建仓库

  - 本地已有仓库
    ```
    git init //进入到本地仓库目录初始化
    或者
    git init 本地仓库目录名称
    ```
  - 本地没有仓库需要clone远程仓库
    ```
    git clone [url] // 克隆项目
    或者
    git clone [url] [目录名] // 克隆项目到一个叫demo的自定义文件夹
    ```

- 查看任意时刻状态命令

  ```
  git status
  ```

- 分支操作

  ```
  git branch //查看分支列表
  git branch 分支名称 //创建分支
  git checkout 分支名称 //切换分支
  【git branch 分支名称 + git checkout 分支名称】 等价于 【git checkout -b [分支名]】 //创建分支同时切换到该分支
  git branch -d [分支名] // 删除本地分支
  ```
  - 合并分支

  ![image](https://s1.ax1x.com/2020/05/28/tZZc9J.png)

  ```
  // 合并分支(切换到目标分支执行，以下分支名为源分支)
  // 如果发生冲突手动解决冲突代码后再提交即可
  // 每次提交代码前先pull后push可以有效的代码在远程仓库冲突，这种做法有点像弃车保帅的方式，车为本地仓库，而帅则为远程仓库
  git merge [分支名]
  ```
- 回滚撤销

  - 未使用git add时
    ```
    git checkout -- 文件名称 //放弃单个文件修改
    git checkout . //放弃所有文件修改
    git checkout COMMITID // 可以切换到某个历史版本所在的分支上
    注意：不要和切换分支命令混淆
    ```
  - 已经使用了git add添加到了待提交列表
    ```
    git reset HEAD^ // 放弃所有文件的提交
    git reset HEAD 文件名称 //放弃某个文件的提交
    ```
  - 已经使用git commit提交到了本地仓库
    ```
    git reset --hard HEAD^ //回退到最新一次提交
    git reset HEAD^ // 此时代码保留，回到git add之前
    git revert COMMITID //撤销指定的版本，撤销也会作为一次提交进行保存
    ```
  - 已经使用git push提交到了远程仓库
    ```
    git reset --hard COMMITID //删除指定的COMMITID
    ```
  - git revert和git reset区别
    ```
    git revert是用一次新的commit来回滚之前的commit，此次提交之前的commit都会被保留

    git reset是回到某次提交，提交及之前的commit都会被保留，但是此COMMITID之后的修改都会被删除
    ```

- 提交到待提交列表

  ```
  git add 文件名称 // 指定具体文件
  git add . // 指定当前所有文件
  ```

- 提交到本地仓库

  ```
  git commit -m '本次修改记录备注信息'

  git add + git commit 等价于 git -am '备注修改信息'
  ```

- 提交到远程仓库

  ```
  git push origin(远程仓库别名) master(分支名称)
  ```

- 添加远程仓库地址

  ```
  git remote add 远程仓库别名 远程仓库地址
  ```

- 显示远程地址列表

  ```
  git remote
  ```

- 显示远程地址详细信息

  ```
  git remote show 远程仓库别名
  ```

- 查看提交日志

  ```
  git log //查看当前分支历史提交记录
  git log --oneline 等价于 git log
  git log -p 文件名称 //可查看该文件以前每一次push的修改内容
  git log --all //查看所有分支的commit历史
  git log --oneline --all //查看所有提交信息，并显示提交信息的第一句话
  git log --all --oneline --graph //查看所有分支图形化的commit历史(oneline 一条提交信息用一行展示)
  ```

- 查看详细日志(改动的内容)

  ```
  git show COMMITID
  ```

- 添加全局用户名配置

  ```
  git config --global user.name 用户名
  ```

- 添加全局email配置

  ```
  git config --global user.email email地址
  ```

- 查看全局配置

  ```
  git config -l
  ```

- 删除本地文件

  ```
  git rm 文件名称 或者 删除磁盘文件然后在add，commit
  ```

- 查看所有本地文件的改动

  ```
  git diff
  ```

- 编写.gitignore忽略文件规则

>忽略操作系统自动生成的文件，比如缩略图等；

>忽略编译生成的中间文件、可执行文件等，也就是如果一个文件是通过另一个文件自动生成的，那自动生成的文件就没必要放进版本库，比如Java编译产生的.class文件；

>忽略你自己的带有敏感信息的配置文件，比如存放口令的配置文件。

- 去掉每次都输入git账号密码

  ```
  git config --global credential.helper store
  ```

- 为某个版本标记tag(tag标签默认是不会被同步或被合并到其他节点的)

  ![image](https://s1.ax1x.com/2020/05/28/tZZtpj.png)
  ```
  git tag -a [标签名] -m "[备注]" // 添加标签(在commit之后使用)
  git tag -a [标签名] -m "[备注]" [结点id] //给指定结点添加标签
  git tag //列出所有标签
  git show [标签名] //查看某个标签的详细信息
  git checkout [标签名] //回溯到标签所在的结点
  git push origin --tags //将本地的tags全部同步到远程
  ```

- 拉取远程仓库代码和提交到远程仓库
  - 每次提交或拉取的时候最好要指定对应的目标分支
  ```
  git pull origin [dev/master]
  git push origin [dev/master]
  ```
