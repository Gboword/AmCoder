- SFTP相关(等价于rz/sz，此方式适用于没有工具的情况下，**前提是保证sftp默认端口22开放**)

  - `lcd 本地文件路径` 进入到本地的某个目录下
  - `cd 远程文件路径` 进入到远程的某个目录下
  - `lpwd` 显示本地的当前目录的路径
  - `pwd` 显示远程的当前目录的路径
  - 这里只介绍常用的，其他的自行查找

  - 演示场景，Windows上传下文件到Linux(**注意绝对路径和相对路径的区别**)
    - 如何进入sftp?
      - 方式一：如果你在终端工具下，可以利用终端的`sftp`选项进入或者使用快捷键`alt + p`进入
      - 方式二：如果你在Windows下使用命令`sftp 用户名@ip地址`，如果不指定目录，默认`/root`下
      ![Windows操作进入远程服务器ftp](https://s1.ax1x.com/2020/06/22/NGmNee.jpg)

    - Windows上传文件到Linux中
      - 使用命令`put Windows文件路径`
      ![windows上传到Linux](https://s1.ax1x.com/2020/06/22/NGnzNj.jpg)

    - Linux文件下载到Windows中
      - 使用命令`get Linux文件路径`
      ![linux下载文件到Windows](https://s1.ax1x.com/2020/06/22/NGuTZF.png)

    - 分别查看远程和本地的当前路径
      ![lpwd-pwd](https://s1.ax1x.com/2020/06/22/NGK3zq.png)
