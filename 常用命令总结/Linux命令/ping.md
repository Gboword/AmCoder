**ping** (win + linux)判断工具软件可确定两台机器间底层 IP 的连接性

```
ping [-dfnqrRv][-c<完成次数>][-i<间隔秒数>][-I<网络界面>][-l<前置载入>][-p<范本样式>][-s<数据包大小>][-t<存活数值>][主机名称或IP地址]

    -t Ping 指定的计算机直到中断。
　　-a 将地址解析为计算机名。
　　-n count 发送 count 指定的 ECHO 数据包数。默认值为 4。
　　-l length 发送包含由 length 指定的数据量的 ECHO 数据包。默认为 32 字节;最大值是65,527。
　　-f 在数据包中发送"不要分段"标志。数据包就不会被路由上的网关分段。
　　-i ttl 将"生存时间"字段设置为 ttl 指定的值。
　　-v tos 将"服务类型"字段设置为 tos 指定的值。
　　-r count 在"记录路由"字段中记录传出和返回数据包的路由。count 可以指定最少 1 台，最多 9 台计算机。
　　-s count 指定 count 指定的跃点数的时间戳。
　　-j computer-list 利用 computer-list 指定的计算机列表路由数据包。连续计算机可以被中间网关分隔(路由稀疏源)IP 允许的最大数量为 9。
　　-k computer-list 利用 computer-list 指定的计算机列表路由数据包。连续计算机不能被中间网关分隔(路由严格源)IP 允许的最大数量为 9。
　　-w timeout 指定超时间隔，单位为毫秒。
```
