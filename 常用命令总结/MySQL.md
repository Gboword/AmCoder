- 导出数据库为SQL文件

  `mysqldump -u用户名 -p密码 数据库名称 > risen.sql`

- 导入SQL文件

  `mysqlimport -u用户名 -p密码 数据库名称 < risen.sql`

  `进入到MySQL，use 数据库名称，执行source SQL文件路径`

- 远程授权

  `grant all privileges on *.* to '用户名'@'%' identified by '登录密码' with grant option;`
  - \*\.\*：**数据库.表名**
  - @%：**任何IP地址**
  - with grant option：**当前授权的用户有权限授权其他用户**

- 刷新表(常用于授权后的操作，即使权限生效)

  `flush privileges`

- 用户远程登录

  `mysql -u用户名 -p密码 -hIP地址 -P端口号`

- 查看表结构

  `desc 表名`

- 创建数据库并指定编码

  - 设置utf-8编码

  `CREATE DATABASE 数据库名称 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;`

  - 设置gbk编码

  `CREATE DATABASE 数据库名称 DEFAULT CHARACTER SET gbk COLLATE gbk_chinese_ci;`

- 删除表

  `drop table 表名`

- 清空表

  `delete from 表名`
