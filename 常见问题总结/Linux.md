- 问题描述

> gcc: error trying to exec 'cc1plus': execvp: No such file or directory

![报错截图](https://s1.ax1x.com/2020/07/24/UvlOqs.png)

**解决方案**

```SHELL
yum install gcc-c++ -y
```

---

- 问题描述

> 服务器插入新的磁盘后，需要我们先进行格式化然后在挂载

**方案解决**

1. 通过使用命令 `fdisk -l` 查看新增的磁盘盘符路径
2. 如果需要挂载的磁盘比较大，那么可以使用`sudo mkfs.ext4 盘符路径`跳过磁盘检查，快速格式化
  - 如果需要格式化的时候检查磁盘是否损坏可以使用命令`sudo mkfs.ext4 -c 盘符路径`
3. 格式化完成后，执行挂载命令`mount 盘符路径 目标目录路径` (**如果目标目录路径不存在需要提前创建**)
