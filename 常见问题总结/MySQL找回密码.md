- 环境准备

  - 具备修改my.cnf权限

  - 常见使用场景是针对那些自带MySQL的操作系统，而此时你正好需要使用MySQL，其他人却又不使用的情况。其他人如果正在使用中时，请使用正规流程申请账号密码操作。

- 开始

  1. 找到MySQL的配置文件，一般系统自带的MySQL配置文件在/etc/mysql/mysql.conf.d/mysqld.cnf位置

  2. 并修改其中的`[mysqld]`组，新增`skip-grant-tables`配置(跳过权限验证)

  3. 重启MySQL服务

  4. 此时可以无密码登录MySQL，`mysql –uroot –p`连续**两次回车**即可登录进去

  5. 登录进来之后当然就是重置密码了
  ```SHELL
    update mysql.user set authentication_string=password('123456') where user='root';

    flush privileges;
  ```

  6. 密码都重置好了，然后根据第1步中的操作删除掉权限验证的配置，不然任何人都可以免密登录了，重启MySQL服务即可
