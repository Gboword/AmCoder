1. 为root设置密码
```SHELL
sudo passwd root
```
2. 切换到root用户
```SHELL
su - root
```
3. 编辑sshd_config文件
```SHELL
# 修改 PermitRootLogin without-password 为
PermitRootLogin yes
# 修改 PasswordAuthentication no 为
PasswordAuthentication yes
# 关闭证书验证登录 UsePAM yes 修改为
UsePAM no
```

4. 重启ssd服务 `sudo service ssh restart`
