# 测试hive新增字段后覆盖原分区，新字段为NULL
1. 建表
2. 插入两个分区数据
3. 增加字段
4. 覆盖分区数据
5. 查看分区数据新增字段是否为空
6. 增加cascade测试是否为空

```SQL
create table test.test_20200714_cascade(
	id string,
	name string)
partitioned by
(dt string);

insert into test.test_20200714_cascade partition(dt='20200713')
(id,name) values('testname1','1');

insert into test.test_20200714_cascade partition(dt='20200714')
(id,name) values('testname2','2');

select * from test.test_20200714_cascade;

alter table test.test_20200714_cascade add columns(media_name string COMMENT '媒介名称');

insert overwrite table test.test_20200714_cascade partition(dt='20200714')
select id,name,'media_name' from test.test_20200714_cascade
where dt = '20200714';

select * from test.test_20200714_cascade;

alter table test.test_20200714_cascade add columns(last_name string) cascade;

insert overwrite table test.test_20200714_cascade partition(dt='20200714')
select id,name,'media_name','last_name' from test.test_20200714_cascade
where dt = '20200714';

select * from test.test_20200714_cascade;
```
