**以下我们在Centos7操作系统上以MySQL5.6为例来进行一下安装**

# 环境准备

1. 可联网的yum源或者本地系统盘(二者任一，此步骤主要是按照编译的必须环境，如果确定已有可跳过)
2. [MySQL5.6](https://cdn.mysql.com//Downloads/MySQL-5.6/mysql-5.6.49.tar.gz)
3. 编译工具[cmake](https://github-production-release-asset-2e65be.s3.amazonaws.com/537699/c0ad4500-a0bf-11ea-8e52-0f83dda5f9b8?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20200714%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200714T090256Z&X-Amz-Expires=300&X-Amz-Signature=7a714b418d3cc51438c3b63dd99a547b84060628eaa32eefc3b3f41319ba1d74&X-Amz-SignedHeaders=host&actor_id=0&repo_id=537699&response-content-disposition=attachment%3B%20filename%3Dcmake-3.17.3.tar.gz&response-content-type=application%2Foctet-stream)

# 开始安装

> 操作用户：root 安装目录：/opt/app

## 安装编译环境

> 如果有gcc等编译环境可跳过

```SHELL
yum install -y  make* gcc* ncurses-devel* gcc-c++* perl* psmisc
```

## 安装cmake命令

1. 上传安装包到服务器上，运行解压命令

```SHELL
tar -zcvf cmake-3.17.3.tar.gz -C /opt/app
```

2. 编译

> 进入到/opt/app/cmake-3.17.3目录下执行命令

```SHELL
./configure
```

3. 安装

> 在上一步所在目录中执行命令（提示：make时如果无法成功可能需要 `make clean` 一下）

```SHELL
make && make install
```

4. 检测是否安装成功

```SHELL
cmake -version
```

## 安装MySQL

1. 创建MySQL用户并将MySQL用户加入到MySQL用户组中

```SHELL
groupadd mysql
useradd -s /sbin/nologin -g mysql mysql
```

2. 创建MySQL数据目录和日志目录

```SHELL
mkdir /data/mysql/{data,log} -p
```

3. 上传安装包到服务器上，运行解压命令

```SHELL
tar -zcvf  mysql-5.6.17.tar.gz -C /opt/app
```

4. 编译

> 进入到/opt/app/mysql-5.6.17目录下执行命令
>> 命令解释：
>>> DCMAKE_INSTALL_PREFIX：类似于configure脚本的 –prefix <br/> DMYSQL_DATADIR：MySQL的数据目录 <br/> DMYSQL_UNIX_ADDR：设置UNIX socket文件 <br/>  DENABLED_LOCAL_INFILE：启用加载本地数据 <br/> DWITH_INNOBASE_STORAGE_ENGINE：启用对InnoDB引擎支持 <br/> DWITH_ARCHIVE_STORAGE_ENGINE：启用对ARCHIVE引擎支持 <br/> DWITH_BLACKHOLE_STORAGE_ENGINE：启用对BLACKHOLE引擎支持<br/>DWITH_EXAMPLE_STORAGE_ENGINE：启用对EXAMPLE引擎支持<br/>DWITH_PARTITION_STORAGE_ENGINE：启用对partition引擎支持 <br/> DWITH_MYISAM_STORAGE_ENGINE：启用对myisam引擎支持  <br/>DWITH_PERFSCHEMA_STORAGE_ENGINE：启用对PERFSCHEMA引擎支持  <br/>
DMYSQL_TCP_PORT：TCP/IP端口 <br/>DEXTRA_CHARSETS：指定扩展字符支持 <br/>DDEFAULT_CHARSET：指定默认字符集 <br/>DDEFAULT_COLLATION：默认字符校对 <br/>DMYSQL_USER：指定mysqld运行用户

```SHELL
cmake -DCMAKE_INSTALL_PREFIX=/opt/app/mysql -DMYSQL_DATADIR=/data/mysql/data -DMYSQL_UNIX_ADDR=/opt/app/mysql/mysql.sock -DENABLED_LOCAL_INFILE=1 -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_ARCHIVE_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_FEDERATED_STORAGE_ENGINE=1 -DWITH_EXAMPLE_STORAGE_ENGINE=1 -DWITH_PARTITION_STORAGE_ENGINE=1 -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_PERFSCHEMA_STORAGE_ENGINE=1 -DMYSQL_TCP_PORT=3306 -DEXTRA_CHARSETS=all -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DMYSQL_USER=mysql
```

5. 安装

> 在上一步所在目录中执行命令，此过程比较长

```SHELL
make && make install
```

6. 修改配置文件

> 修改配置文件/etc/my.cnf

```SHELL
[mysqld]
datadir=/data/mysql/data
socket =/opt/app/mysql/mysql.sock
lower_case_table_names=1
character-set-server=utf8
collation-server=utf8_general_ci
skip-name-resolve

[client]
default-character-set=utf8
socket =/opt/app/mysql/mysql.sock

[mysql.server]
user=mysql
basedir=/opt/app/mysql
default-character-set=utf8

[mysqld_safe]
err-log=/data/mysql/log/mysqld.log
```

7. 修改日志目录和数据目录以及安装目录的权限

```SHELL
chown -R mysql.mysql /data/mysql/
chown -R mysql.mysql /opt/app/mysql
```

8. 初始化MySQL

> 进入到/usr/local/mysql/scripts执行命令

```SHELL
./mysql_install_db --user=mysql --basedir=/opt/app/mysql --datadir=/data/mysql/data
```

9. 添加MySQL服务

> 进入到/usr/local/mysql/support-files执行以下命令

```SHELL
cp mysql.server /etc/rc.d/init.d/mysql
```

10. 设置开机启动MySQL

```SHELL
chkconfig --add mysql
chkconfig mysql on
```

11. 启动MySQL服务

```SHELL
service mysql start
```

12. 将MySQL命令加入到环境变量

```SHELL
ln -s /opt/app/mysql/bin/* /usr/bin/
```

13. 设置初始化密码并登陆

```SHELL
mysqladmin -u root password '123456'
```

> 到此，MySQL正常安装步骤已经完毕，接下来就是配置远程登录权限了，参考【常见命令总结】中的【MySQL】即可
