- 准备环境
  - 可用的Python环境
  - NGINX环境

# 开始安装

1. 首先安装包pip2pi

2. 创建镜像源仓库位置目录
  ```
  mkdir /data/pip3_source
  ```
3. 将需要的依赖包上传至上述目录下

4. 建立依赖包索引
  ```
  dir2pi /data/pip3_source

  此时会在/data/pip3_source目录下生成一个simple的目录
  ```

5. 配置nginx添加以下反向代理
  ```
  server {
          listen       8000;
          server_name  192.169.5.59:8000;
          charset utf-8,gbk;
          location / {
                  root /data/pip3_source;
                  autoindex on;
                  autoindex_exact_size off; #显示文件的大小
                  autoindex_localtime on; #显示文件时间
                  #limit_rate_after 5m; #5分钟后下载速度限制为200k
                  #limit_rate 200k;
          }
          access_log logs/pip.log;
  }
  ```

6. 进入到NGINX安装目录下bin目录下运行nginx
```
./nginx
```

7. 浏览器验证

  http://192.168.5.59:8000/simple

  ![结果](https://s1.ax1x.com/2020/06/05/trYAVs.jpg)

8. 通过读取本地镜像源安装flask模块，需要加入**--trusted-host 192.168.5.59**
  ```
  pip3 install flask --index-url=http://192.168.5.59:8000/simple/
  ```
  ![安装成功](https://s1.ax1x.com/2020/06/05/trYcJP.jpg)
